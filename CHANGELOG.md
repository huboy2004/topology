# v0.0.3 (2019.9.20)

## Features

- 新增时序图基本图形
- 新增类图基本图形
- 新增基础图形 - people
- 输入框回车换行调整
- 属性输入支持点击空白更新
- 导航菜单文案和内容调整

## Improvements

- 复合（父子）图形支持
- 增加 canvas 选项
- render()函数拆分，使其更纯粹；新增 open 函数
- 部分结构/函数优化

## Bug Fixes

- 打开本地文件可能需要点击 2 次
- 支持文字回车换行
- 多节点旋转偏移
